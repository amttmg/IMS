<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                "name"         => 'admin',
                "display_name" => 'Administrator',
                "description"  => 'Super Admin',
            ],
            [
                "name"         => 'company',
                "display_name" => 'Company',
                "description"  => 'Company Holders',
            ],
            [
                "name"         => 'student',
                "display_name" => 'Student',
                "description"  => 'Student',
            ],
        ];
        foreach ($roles as $role) {
            if (!Role::where('name', '=', $role['name'])->exists()) {
                Role::create($role);
            }
        }
    }
}
