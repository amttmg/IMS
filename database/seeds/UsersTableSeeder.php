<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole   = Role::where('name', 'admin')->first();
        $companyRole = Role::where('name', 'company')->first();

        $adminUser   = [
            'name'     => 'admin',
            'email'    => 'admin@admin.com',
            'password' => bcrypt('password'),
        ];
        $companyUser = [
            'name'     => 'company',
            'email'    => 'company@admin.com',
            'password' => bcrypt('password'),
        ];
        if (!User::where('email', '=', $adminUser['email'])->exists()) {
            $admin = User::create($adminUser);
            $admin->attachRole($adminRole);
        }
        if (!User::where('email', '=', $companyUser['email'])->exists()) {
            $company = User::create($companyUser);
            $company->attachRole($companyRole);
        }
    }
}
