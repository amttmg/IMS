<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Zizaco\Entrust\Entrust;

Auth::routes();

Route::get('/', function () {
    if (app(Entrust::class)->hasRole('admin')) {
        return redirect()->route('admin');
    } elseif (app(Entrust::class)->hasRole('company')) {
        return redirect()->route('company');
    } else {
        return redirect()->route('login');
    }
});
Route::get('/administrator', 'Main\MainController@admin')
    ->middleware(['role:admin'])
    ->name('admin');
Route::get('/company', 'Main\MainController@company')
    ->middleware(['role:company'])
    ->name('company');

