<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware([])->namespace('Api\Admin')->group(function () {
    Route::post('/users/get-data', 'UsersController@getData')->name('users.data');
    Route::resource('users', 'UsersController');
    Route::post('/company/get-data', 'CompanyController@getData')->name('company.data');
    Route::resource('company', 'CompanyController');
});

Route::middleware([])->namespace('Api\Company')->group(function () {

});
