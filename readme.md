# IMS (Institute Management System)

## Introduction

> A complete software for Institute / Colleges / Tution centers.


## Installation

- `git clone https://gitlab.com/amttmg/IMS.git`
- `composer install`
- `cp .env.example .env`
- setup your database on .env file
- `php artisan migrate`
- `php artisan db:seed`
- `npm run dev`
- `php artisan serve`
- Lets Rock.