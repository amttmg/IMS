import DashboardLayout from '../components/Dashboard/Layout/DashboardLayout.vue'
// GeneralViews
import NotFound from '../components/GeneralViews/NotFoundPage.vue'

// Admin pages
import Overview from '../components/Dashboard/Views/Overview.vue';
import UsersList from '../components/Dashboard/Views/Admin/Users/index.vue';
import UserCreate from '../components/Dashboard/Views/Admin/Users/Create.vue';
import UserEdit from '../components/Dashboard/Views/Admin/Users/Edit.vue';
import CompanyList from '../components/Dashboard/Views/Admin/Company/index.vue';
import CompanyCreate from '../components/Dashboard/Views/Admin/Company/Create.vue';
import CompanyEdit from '../components/Dashboard/Views/Admin/Company/Edit.vue';
const routes = [
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/overview'
  },
  {
    path: '/',
    component: DashboardLayout,
    redirect: '/stats',
    children: [
      {
        path: 'overview',
        name: 'overview',
        component: Overview
      },
      {
        path: 'users',
        name: 'users',
        component: UsersList
      },
      {
        path: 'users/create',
        name: 'Create User',
        component: UserCreate
      },
      {
        path: 'users/edit/:id',
        name: 'Edit User',
        component: UserEdit
      },
      {
        path: 'company',
        name: 'companies',
        component: CompanyList
      },
      {
        path: 'company/create',
        name: 'Create Company',
        component: CompanyCreate
      },
      {
        path: 'company/edit/:id',
        name: 'Edit Company',
        component: CompanyEdit
      },
    ]
  },
  {path: '*', component: NotFound}
]

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
 function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes
