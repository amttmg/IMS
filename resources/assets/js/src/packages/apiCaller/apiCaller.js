import axios from 'axios';
export default function(Vue) {
  Vue.request = {
    post: (url, data = {}) => {
      console.log("from package post");
      return axios({
        url: url,
        data: data,
        method: 'post'
      })
    },
    get: (url, data = {}) => {
      return axios({
        url: url,
        data: data,
        method: 'get'
      })
    },
    send: (url, data = {}, method) => {
      return axios({
        url: url,
        data: data,
        method: method
      })
    }
  };
  Object.defineProperties(Vue.prototype, {
    $http: {
      get: () => {
        return Vue.request;
      }
    }
  })
}