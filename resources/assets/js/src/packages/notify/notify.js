export default function(Vue) {
  Vue.prototype.$notify = function(message, type, icon = 'fa fa-check') {
    let self = this;
    self.$notifications.notify(
      {
        message: message,
        icon: icon,
        horizontalAlign: 'right',
        verticalAlign: 'top',
        type: type
      })
  };
}