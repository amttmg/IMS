import fgInput from './components/UIComponents/Inputs/formGroupInput.vue';
import DropDown from './components/UIComponents/Dropdown.vue';
import Box from './components/UIComponents/Box.vue';
import DataTable from './components/UIComponents/DataTable/DataTable.vue';

/**
 * You can register global components here and use them as a plugin in your main Vue instance
 */

const GlobalComponents = {
  install (Vue) {
    Vue.component('fg-input', fgInput);
    Vue.component('drop-down', DropDown);
    Vue.component('data-table', DataTable);
    Vue.component('box', Box);
  }
};

export default GlobalComponents
