import Vue from 'vue';
import VeeValidate from 'vee-validate';
import Datatable from 'vue2-datatable-component'
import VueRouter from 'vue-router';

// Plugins
import GlobalComponents from './globalComponents'
import GlobalDirectives from './globalDirectives'
import Notifications from './components/UIComponents/NotificationPlugin'
import SideBar from './components/UIComponents/SidebarPlugin/admin'
import apiCaller from './packages/apiCaller/apiCaller';
import notify from './packages/notify/notify';
import validation from './packages/serverValidation/validation';
import App from './App'

// router setup
import routes from './routes/admin';

// library imports
import Chartist from 'chartist'
import 'bootstrap/dist/css/bootstrap.css'
import './assets/sass/paper-dashboard.scss'
import 'es6-promise/auto';
import 'expose-loader?$!expose-loader?jQuery!jquery';

// plugin setup
Vue.use(VueRouter);
Vue.use(GlobalComponents);
Vue.use(GlobalDirectives);
Vue.use(Notifications);
Vue.use(SideBar);
Vue.use(VeeValidate);
Vue.use(Datatable); // done!
Vue.use(apiCaller);
Vue.use(notify);
Vue.use(validation);


// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkActiveClass: 'active'
});

// global library setup
Object.defineProperty(Vue.prototype, '$Chartist', {
  get () {
    return this.$root.Chartist
  }
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router,
  data: {
    Chartist: Chartist
  }
});
