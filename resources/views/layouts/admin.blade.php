<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <title>Administrator</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link href="static/css/themify-icons.css" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  {{ csrf_field() }}
</form>
@php
  $routes     = app('router')->getRoutes();
          $routeArray = [];
          foreach ($routes as $r) {
              $routeArray[$r->getName()] = $r->uri;
          }
@endphp
<script>
  let routes = JSON.parse('<?php echo json_encode($routeArray) ?>');
</script>
<div class="wrapper" id="app">

</div>
<script src="{{ asset('js/admin.js') }}"></script>
</body>
</html>
