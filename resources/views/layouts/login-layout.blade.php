<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8"/>
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('static/img/apple-icon.png') }}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('static/img/favicon.png') }}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

  <title>PROJECT V1 Login</title>

  <!-- Canonical SEO -->
  <link rel="canonical" href="http://www.creative-tim.com/product/paper-dashboard-pro"/>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
  <meta name="viewport" content="width=device-width"/>

  <link href="{{ asset('css/paper.css') }}" type="text/css" rel="stylesheet">
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
  <script src="{{ asset('js/front.js') }}"></script>
  <style type="text/css">
    body {
      background-image: url('{{ asset('static/img/bg.jpg') }}');
      background-size: cover;
    }
  </style>
</head>

<body>
<nav class="navbar navbar-transparent navbar-absolute">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">PROJECT V1 Login</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li>
          <a href="{{ route('login') }}">
            Login
          </a>
        </li>
        <li>
          <a href="{{ route('register') }}">
            Register
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="wrapper wrapper-full-page">
  <div class="full-page login-page bgimg" data-color="" data-image="{{ asset('static/img/bg.jpg') }}">
    <!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
    @yield('content')

    <footer class="footer footer-transparent">
      <div class="container">
        <div class="copyright">
          &copy;
          <script>document.write(new Date().getFullYear())</script>
          , made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com">Creative Team</a>
        </div>
      </div>
    </footer>
  </div>
</div>
</body>
</html>
