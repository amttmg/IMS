@extends('layouts.login-layout')

@section('content')
  {{--<div class="container">
      <div class="row">
          <div class="col-md-8 col-md-offset-2">
              <div class="panel panel-default">
                  <div class="panel-heading">Register</div>

                  <div class="panel-body">
                      <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                          {{ csrf_field() }}

                          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                              <label for="name" class="col-md-4 control-label">Name</label>

                              <div class="col-md-6">
                                  <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>

                                  @if ($errors->has('name'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('name') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                              <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                              <div class="col-md-6">
                                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                  @if ($errors->has('email'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                              <label for="password" class="col-md-4 control-label">Password</label>

                              <div class="col-md-6">
                                  <input id="password" type="password" class="form-control" name="password" required>

                                  @if ($errors->has('password'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>

                          <div class="form-group">
                              <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                              <div class="col-md-6">
                                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                              </div>
                          </div>

                          <div class="form-group">
                              <div class="col-md-6 col-md-offset-4">
                                  <button type="submit" class="btn btn-primary">
                                      Register
                                  </button>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>--}}
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
          <div class="card" data-background="color" data-color="blue">
            <form class="" method="POST" action="{{ route('register') }}">
              {{ csrf_field() }}
              <div class="card-header">
                <h3 class="card-title">Register</h3>
              </div>
              <div class="card-content">
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                  <label>Name</label>
                  <input type="text" placeholder="Enter Name" name="name" class="form-control border-input">
                  @if ($errors->has('name'))
                    <span class="help-block">
                     {{ $errors->first('name') }}
                    </span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                  <label>Email address</label>
                  <input type="email" placeholder="Enter email" name="email" class="form-control border-input">
                  @if ($errors->has('email'))
                    <span class="help-block">
                     {{ $errors->first('email') }}
                    </span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                  <label>Password</label>
                  <input type="password" placeholder="Password" name="password" class="form-control border-input">
                  @if ($errors->has('password'))
                    <span class="help-block">
                     {{ $errors->first('email') }}
                   </span>
                  @endif
                </div>
                <div class="form-group">
                  <label>Password Confirmation</label>
                  <border-inputsword" placeholder="Password Confirmation" name="password_confirmation" class="form-control border-input">
                </div>
              </div>
              <div class="card-footer text-center">
                <button type="submit" class="btn btn-fill btn-wd ">Register</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
