@extends('layouts.login-layout')
@section('content')
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
          <div class="card" data-background="color" data-color="blue">
            <form class="" method="POST" action="{{ route('login') }}">
              {{ csrf_field() }}
              <div class="card-header">
                <h3 class="card-title">Login</h3>
              </div>
              <div class="card-content">
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                  <label>Email address</label>
                  <input type="email" placeholder="Enter email" name="email" class="form-control border-input">
                  @if ($errors->has('email'))
                    <span class="help-block">
                     {{ $errors->first('email') }}
                    </span>
                  @endif
                </div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                  <label>Password</label>
                  <input type="password" placeholder="Password" name="password" class="form-control border-input">
                  @if ($errors->has('password'))
                    <span class="help-block">
                     {{ $errors->first('password') }}
                   </span>
                  @endif
                </div>
              </div>
              <div class="card-footer text-center">
                <button type="submit" class="btn btn-fill btn-wd ">Let's go</button>
                <div class="forgot">
                  <a href="#pablo">Forgot your password?</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
