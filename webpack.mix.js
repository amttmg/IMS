let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/src/company.js', 'public/js')
  .js('resources/assets/js/src/front.js', 'public/js')
  .js('resources/assets/js/src/admin.js', 'public/js');

//.sass('resources/assets/js/src/assets/sass/paper-dashboard.scss', 'public/css');
