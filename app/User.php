<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * Class User
 * @package App
 */
class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function company()
    {
        return $this->hasMany(Company::class);
    }

    /**
     * @param $query
     * @return \Zizaco\Entrust\Traits\users
     */
    public function scopeAdmin($query)
    {
        return $this->scopeWithRole($query, 'admin');
    }

    /**
     * @param $query
     * @return \Zizaco\Entrust\Traits\users
     */
    public function scopeCompany($query)
    {
        return $this->scopeWithRole($query, 'company');
    }
}
