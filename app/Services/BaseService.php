<?php

namespace App\Services;


class BaseService implements BaseInterface
{

    /**
     * Get all
     *
     * @return mixed
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Find by primary key
     *
     * @param int $id
     * @return mixed
     */
    public function find(int $id)
    {
        return $this->model->find($id);
    }

    /**
     * create
     *
     * @param $storeData
     * @return mixed
     */
    public function create(array $storeData)
    {
        return $this->model->create($storeData);
    }

    /**
     * update
     *
     * @param $updateData
     * @param $id
     * @return mixed
     */
    public function update(array $updateData, int $id)
    {
        $model = $this->find($id);

        $model->update($updateData);

        return $model;
    }

    /**
     * destroy
     *
     * @param int $id
     * @return mixed
     */
    public function destroy(int $id)
    {
        $model = $this->find($id);
        $model->delete($model);
    }
}