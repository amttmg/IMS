<?php

namespace App\Services;

use App\Company;
use App\Role;
use App\User;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\DB;

/**
 * Class UserService
 * @package App\Services
 */
class CompanyService extends BaseService
{
    /**
     * @var User
     */
    protected $model;
    /**
     * @var User
     */
    private $userService;

    /**
     * UserService constructor.
     * @param Company     $company
     * @param UserService $userService
     * @internal param User $user
     */
    public function __construct(Company $company, UserService $userService)
    {
        $this->model = $company;
        $this->userService  = $userService;
    }

    /**
     * @param array $requestData
     * @return array
     */
    public function getData(array $requestData)
    {
        $user = $this->model->query();
        if (isset($requestData['filter'])) {
            $user->where('name', 'like', '%'.$requestData['filter'].'%');
        }
        $total = $user->count();
        if ($requestData['sort']) {
            $user->orderBy($requestData['sort'], $requestData['order']);
        }
        if ($requestData['limit']) {
            $user->limit($requestData['limit']);
        }
        if ($requestData['offset']) {
            $user->offset($requestData['offset']);
        }

        return [
            'data'  => $user->get(),
            'total' => $total,
        ];
    }

    /**
     * @param array $storeData
     * @return mixed|void
     */
    public function create(array $storeData)
    {
        DB::transaction(function () use ($storeData){
            $storeData['password'] = bcrypt('password');
            $storeData['status']   = 'active';
            $storeData['logo']     = $storeData['logo'] ?? 'default';

            $user = $this->userService->createCompany($storeData);
            return $user->company()->create($storeData);
        });
    }
}
