<?php

namespace App\Services;

use App\Role;
use App\User;

/**
 * Class UserService
 * @package App\Services
 */
class UserService extends BaseService
{
    /**
     * @var User
     */
    protected $model;

    /**
     * UserService constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function getData(array $requestData)
    {
        $user = $this->model->admin();
        if (isset($requestData['filter'])) {
            $user->where('name', 'like', '%'.$requestData['filter'].'%');
        }
        $total = $user->count();
        if ($requestData['sort']) {
            $user->orderBy($requestData['sort'], $requestData['order']);
        }
        if ($requestData['limit']) {
            $user->limit($requestData['limit']);
        }
        if ($requestData['offset']) {
            $user->offset($requestData['offset']);
        }

        return [
            'data'  => $user->get(),
            'total' => $total,
        ];
    }

    public function createAdmin(array $userData)
    {
        $newUser   = parent::create($userData);
        $adminRole = Role::where('name', 'admin')->first();
        $newUser->attachRole($adminRole);

        return $newUser;
    }

    public function createCompany(array $userData)
    {
        $newUser   = parent::create($userData);
        $adminRole = Role::where('name', 'company')->first();
        $newUser->attachRole($adminRole);

        return $newUser;
    }
}
