<?php

namespace App\Services;

/**
 * Interface BaseInterface
 * @package App\Services
 */
interface BaseInterface
{
    /**
     * Get all
     *
     * @return mixed
     */
    public function all();

    /**
     * Find by primary key
     *
     * @param int $id
     * @return mixed
     */
    public function find(int $id);

    /**
     * create
     *
     * @param $storeData
     * @return mixed
     */
    public function create(array $storeData);

    /**
     * update
     *
     * @param $updateData
     * @param $id
     * @return mixed
     */
    public function update(array $updateData, int $id);

    /**
     * destroy
     *
     * @param int $id
     * @return mixed
     */
    public function destroy(int $id);
}
