<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * @var User
     */
    protected $userService;

    /**
     * UsersController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function getData()
    {
        return $this->userService->getData(request()->all());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $this->userService->createAdmin($request->all());

        return response()->json([
            'status'  => true,
            'message' => 'successfully saved',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->userService->find($id);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest|Request $request
     * @param  int                $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user                   = $this->userService->find($id);
        $updateData             = $request->all();
        $updateData['password'] = $updateData['password'] ? bcrypt($updateData['password']) : $user->password;
        $user->update($updateData);

        return response()->json([
            'status'  => true,
            'message' => 'Successfully Updated',
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->userService->find($id);
        $user->distroy();

        return response()->json([
            'status'  => true,
            'message' => 'successfully saved',
        ]);
    }
}
