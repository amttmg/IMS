<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;

/**
 * Class MainController
 * @package App\Http\Controllers\Main
 */
class MainController extends Controller
{
    /**
     * Show the application admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin()
    {
        return view('layouts.admin');
    }

    /**
     * Show company Dashboard
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function company()
    {
        return view('layouts.company');
    }
}
